package com.stoliarenko.freezer.service;

import java.util.List;

import com.stoliarenko.freezer.entity.Food;

public interface FoodService {

	public List<Food> getFoods();

	public void saveFood(Food theFood);

	public Food getFood(int theId);

	public void deleteFood(int theId);

	public List<Food> getFoodByName(String foodName);

	
}
