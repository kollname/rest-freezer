package com.stoliarenko.freezer.service;

import java.util.List;

import com.stoliarenko.freezer.dao.FoodDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stoliarenko.freezer.entity.Food;

@Service
public class FoodServiceImpl implements FoodService {

	// need to inject customer dao
	@Autowired
	private FoodDAO foodDAO;
	
	@Override
	@Transactional
	public List<Food> getFoods() {
		return foodDAO.getFoods();
	}

	@Override
	@Transactional
	public void saveFood(Food theFood) {

		foodDAO.saveFood(theFood);
	}

	@Override
	@Transactional
	public Food getFood(int theId) {
		
		return foodDAO.getFood(theId);
	}

	@Override
	@Transactional
	public void deleteFood(int theId) {
		
		foodDAO.deleteFood(theId);
	}

	@Override
	@Transactional
	public List<Food> getFoodByName(String foodName) {
		return foodDAO.getFoodByName(foodName);
	}
}





