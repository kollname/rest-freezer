package com.stoliarenko.freezer.rest;

import com.stoliarenko.freezer.entity.Food;
import com.stoliarenko.freezer.rest.exception_handlers.FoodNotFoundException;
import com.stoliarenko.freezer.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FoodRestController {

    @Autowired
    FoodService foodService;

    @GetMapping("/food")
    public List<Food> getFoods(){

        return foodService.getFoods();
    }

    @GetMapping("/food/{foodId}")
    public String  getFood(@PathVariable int foodId){
        Food food = foodService.getFood(foodId);
        if(food == null){
            throw new FoodNotFoundException("Food id not found - " + foodId);
        }

        return food.getDescription();
    }

    @PostMapping("/food")
    public int addFood(@RequestBody Food food){
        food.setId(0);
        foodService.saveFood(food);

        return food.getId();
    }

    @PutMapping("/food")
    public Food updateFood(@RequestBody Food food){
        foodService.saveFood(food);

        return food;
    }
    @DeleteMapping("/food/{foodId}")
    public String deleteFood(@PathVariable int foodId){
        Food food = foodService.getFood(foodId);

        if(food == null){
            throw new FoodNotFoundException("Customer not found id - " + foodId);
        }
        foodService.deleteFood(foodId);


        return "Deleted customer id " + foodId;
    }

    @GetMapping("/food/search/{foodName}")
    public List<Food>  getFoodByName(@PathVariable String foodName){
        List<Food> foods = foodService.getFoodByName(foodName);
        if(foods == null){
            throw new FoodNotFoundException("Food id not found - " + foodName);
        }

        return foods;
    }
}
