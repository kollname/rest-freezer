package com.stoliarenko.freezer.rest.exception_handlers;

public class FoodNotFoundException extends RuntimeException {

    public FoodNotFoundException(String message) {
        super(message);
    }

    public FoodNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FoodNotFoundException(Throwable cause) {
        super(cause);
    }
}
