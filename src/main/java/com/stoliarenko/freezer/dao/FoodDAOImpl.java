package com.stoliarenko.freezer.dao;

import java.util.List;

import com.stoliarenko.freezer.entity.Food;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FoodDAOImpl implements FoodDAO {

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
			
	@Override
	public List<Food> getFoods() {
		
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
				
		// create a query  ... sort by last name
		Query<Food> theQuery =
				currentSession.createQuery("from Food order by name",
											Food.class);
		
		// execute query and get result list
		List<Food> foods = theQuery.getResultList();
				
		// return the results		
		return foods;
	}

	@Override
	public List<Food> getFoodByName(String foodName) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

//		// create a query  ... sort by last name
		Query<Food> theQuery =
				currentSession.createQuery("from Food where name=:foodName order by name",
						Food.class);

		theQuery.setParameter("foodName", foodName);

		// execute query and get result list
		List<Food> foods = theQuery.getResultList();

		// return the results
		return foods;

	}

	@Override
	public void saveFood(Food theFood) {

		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// save/upate the customer ... finally LOL
		currentSession.saveOrUpdate(theFood);
		
	}

	@Override
	public Food getFood(int theId) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// now retrieve/read from database using the primary key
		Food theFood = currentSession.get(Food.class, theId);
		
		return theFood;
	}

	@Override
	public void deleteFood(int theId) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// delete object with primary key
		Query theQuery = 
				currentSession.createQuery("delete from Food where id=:foodId");
		theQuery.setParameter("foodId", theId);
		
		theQuery.executeUpdate();		
	}



}











