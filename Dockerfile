FROM tomcat:9.0
LABEL maintainer="kollnameshop@gmail.com"
ADD target/freezer.war /usr/local/tomcat/webapps/
EXPOSE 8088
CMD ["catalina.sh","run"]